package com.tetroware.zerohour;

import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class App extends BasicGame {

    static Image backgroundimage, redsoldier, bluesoldier, bullet;// Images
    static short playerX = 400, playerY = 318;// Player position
    static boolean playerDirection = false; // Direction (true==left, false==right)
    static byte playerHealth = 10; // Health (max 10, 0==dead);
    static boolean playerBulletDelay = true; // Can fire? (delay)
    static boolean isEnemyOnLeft, isEnemyOnRight; // Is an enemy on (left|right)?
    static byte leftEnemyHealth, rightEnemyHealth; // enemy health
    static boolean leftEnemyBulletDelay = true, rightEnemyBulletDelay = true; // Can enemy fire?
    static int playerBulletX = 0; // Player bullet x
    static boolean playerBulletDirection = false; // Player bullet direction (true==left, false==right)
    static int leftEnemyBulletX = 0, rightEnemyBulletX = 0; // Enemy bullet x (left|right) 

    public App() {
        super("Hardcore Army Island");
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        gc.setShowFPS(false);
        gc.setVSync(true);
        backgroundimage = new Image("background.png");
        redsoldier = new Image("redsoldier.png");
        bluesoldier = new Image("bluesoldier.png");
        bullet = new Image("bullet.png");
    }
    private static final int DELAY = 1000; // 1 sec
    private int elapsedTime; // The time that has passed, reset to 0 after +-1 sec.
    static Random randomNumberGenerator;

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        Input in = gc.getInput();
        if (in.isKeyDown(Input.KEY_A)) { // LEFT
            if (playerX > 60) {
                playerX -= (short) (delta * 0.2f);
            }
            playerDirection = true;
        } else // Don't want to go left and right
        if (in.isKeyDown(Input.KEY_D)) { // RIGHT
            if (playerX < 720) {
                playerX += (short) (delta * 0.2f);
            }
            playerDirection = false;
        }
        if (in.isKeyDown(Input.KEY_SPACE)) {// fire ammo
            if (playerBulletDelay) {
                playerBulletDelay = false;
                playerBulletX = playerX;
                playerBulletDirection = playerDirection;
            }
        }
        if (playerBulletX != 0) {
            if (playerBulletDirection) { // Moving left
                playerBulletX -= (int) (delta * 0.5f);
                if (playerBulletX < 1) {
                    playerBulletX = 0;
                }
                if (isEnemyOnLeft) {// person on left to kill
                    if (playerBulletX <= 60) {// bullet location is on left?
                        leftEnemyHealth -= 3;// reduce health
                        playerBulletX = 0;// destroy bullet
                    }
                }
            } else {
                playerBulletX += (int) (delta * 0.5f);
                if (playerBulletX > 800) {
                    playerBulletX = 0;
                }
                if (isEnemyOnRight) {
                    if (playerBulletX >= 720) {
                        rightEnemyHealth -= 3;
                        playerBulletX = 0;
                    }
                }

            }
        }
        if (leftEnemyBulletX != 0) {
            leftEnemyBulletX += (int) (delta * 0.5f);
            if (leftEnemyBulletX > 800) {
                leftEnemyBulletX = 0;
            }
            if (leftEnemyBulletX >= playerX) {
                playerHealth -= 3;
                leftEnemyBulletX = 0;
            }
        }
        if (rightEnemyBulletX != 0) {
            rightEnemyBulletX -= (int) (delta * 0.5f);
            if (rightEnemyBulletX < 1) {
                rightEnemyBulletX = 0;
            }
            if (rightEnemyBulletX <= playerX) {// bullet location is past player
                playerHealth -= 3;// reduce health
                rightEnemyBulletX = 0;// destroy bullet
            }

        }
        if (leftEnemyHealth <= 0) {
            isEnemyOnLeft = false;
        }
        if (rightEnemyHealth <= 0) {
            isEnemyOnRight = false;
        }
        elapsedTime += delta;
        if (elapsedTime >= DELAY) {//every sec
            elapsedTime = 0;
            playerBulletDelay = true;
            leftEnemyBulletDelay = true;
            rightEnemyBulletDelay = true;
            if (randomNumberGenerator.nextInt(10) == 5) {// 1 in 10 chance of enemy spawn
                int side = randomNumberGenerator.nextInt(2);
                switch (side) {
                    case 0:
                        if (!isEnemyOnLeft) {
                            isEnemyOnLeft = true;
                            leftEnemyHealth = 10;
                        }
                        break;
                    case 1:
                        if (!isEnemyOnRight) {
                            isEnemyOnRight = true;
                            rightEnemyHealth = 10;
                        }
                        break;
                }
            }
            if (randomNumberGenerator.nextInt(4) == 2) {// 1 in 4 chance of fire from enemy
                int side = randomNumberGenerator.nextInt(2);
                switch (side) {
                    case 0:
                        if (isEnemyOnLeft && leftEnemyBulletX == 0) {
                            leftEnemyBulletX = 60;
                        }
                        break;
                    case 1:
                        if (isEnemyOnRight && rightEnemyBulletX == 0) {
                            rightEnemyBulletX = 720;
                        }
                        break;
                }
            }

        }

    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        if (playerHealth <= 0) {//dead
            g.setColor(Color.white);
            g.drawString("YOLO", 380, 280);
            return;
        }
        g.setBackground(Color.black);
        g.drawImage(backgroundimage, 0, 0); // draw backdrop
        // <FPS>
        g.setColor(Color.black);
        String fps = new Integer(gc.getFPS()).toString();
        g.drawString("FPS: " + fps, 0, 0);
        // </FPS>
        g.drawString("Player Health: " + playerHealth, 0, 20);
        if (playerDirection) { //(is facing left)
            g.drawImage(redsoldier, playerX, playerY);// Player
        } else {
            g.drawImage(redsoldier, playerX, playerY, 40, 0, 0, 60);
        }
        // Render enemies
        if (isEnemyOnLeft) {
            g.drawImage(bluesoldier, 60, 318, 40, 0, 0, 60);
            if (leftEnemyBulletX > 0) {
                g.drawImage(bullet, leftEnemyBulletX, 347);
            }
        }
        if (isEnemyOnRight) {
            g.drawImage(bluesoldier, 720, 318);
            if (rightEnemyBulletX > 0) {
                g.drawImage(bullet, rightEnemyBulletX, 347);
            }
        }
        if (playerBulletX != 0) {
            g.drawImage(bullet, playerBulletX, 347);
        }

    }

    public static void main(String[] args) throws SlickException {
        randomNumberGenerator = new Random();
        System.out.println("Working Directory = "
                + System.getProperty("user.dir"));
        System.setProperty("java.library.path", System.getProperty("user.dir"));
        AppGameContainer app = new AppGameContainer(new App());
        app.setDisplayMode(800, 600, false);
        app.start();
    }
}